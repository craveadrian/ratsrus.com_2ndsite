<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft">
					<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="Main Logo"> </a>
				</div>
				<div class="hdRotate">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("palm-beach-county"); ?> <?php $this->helpers->isActiveMenu("broward-county"); ?> <?php $this->helpers->isActiveMenu("martin-county"); ?>><p>SERVICE AREA</p>
							<li <?php $this->helpers->isActiveMenu("palm-beach-county"); ?>><a href="<?php echo URL ?>palm-beach-county"><img src="public/images/arrow.png" alt=""> <span>Palm beach county</span></a></li>
							<li <?php $this->helpers->isActiveMenu("broward-county"); ?>><a href="<?php echo URL ?>broward-county"><img src="public/images/arrow.png" alt=""> <span>Broward County</span></a></li>
							<li <?php $this->helpers->isActiveMenu("martin-county"); ?>><a href="<?php echo URL ?>martin-county"><img src="public/images/arrow.png" alt=""> <span>Martin County</span></a></li>
							</li>
							</li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="clearfix">

		</div>
	</header>

	<!-- <?php if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<h1>BANNER</h1>
			</div>
		</div>
	<?php endif; ?> -->
