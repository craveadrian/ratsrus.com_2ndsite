<div id="section1">
	<div class="row">
		<div class="s1Left col-6 fl">
			<section>
				<h2>Rodent Exclusions to Protect Your Home or Business From Infestations</h2>
				<p>Rodents are not animals you want invading your home or business. Rats and mice spread diseases, which pose a serious risk to human health. These diseases can be transferred to humans through the consumption of food or water or breathing in dust contaminated by rodent droppings and urine, as well as indirectly through ticks, mites, and fleas. </p>
			</section>
		</div>
		<div class="s1Right col-6 fl">
			<img src="public/images/content/img1.jpg" alt="mouse">
		</div>
		<div class="clearfix">
		</div>
	</div>
</div>
<div id="section2">
	<div class="row">
		<div class="s2Left col-6 fl">
			<img src="public/images/content/img2.jpg" alt="mouse">
		</div>
		<div class="s2Right col-6 fl">
			<section>
				<h1>ABOUT US</h1>
				<p>Rats R Us Rodent and Rat Removal wants you to be 100% satisfied with our service because getting your home rodent free means peace of mind. A safe and healthy home or business is paramount to any successful life, and a rodent problem can seriously interrupt that.</p>
				<p>Trust the experts at Rats R Us Rodent and Rat Removal to properly diagnose and tackle any rat or mouse problem that may arise and stay proactive by tackling the pest prevention front. Best of all, we offer FREE estimates for any of our rodent control services and our competitive rates and quality service means you’re in good hands.</p>
				<a href="<?php echo URL ?>" class="btn">LEARN MORE</a>
				<div class="imageBox">
					<img src="public/images/content/abtImg1.jpg" alt="trapped rat">
					<img src="public/images/content/abtImg2.jpg" alt="trapped rat">
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section3">
	<div class="row">
		<div class="s3Left col-6 fl">
			<img src="public/images/content/img3.jpg" alt="mouse">
		</div>
		<div class="s3Right col-6 fl">
			<section>
				<h1>OUR SERVICE</h1>
				<p>While both mice and rats might seem harmless, they can easily spread germs and disease to your property and are a health hazard to be dealt with immediately. Hiring a professional exterminator for your rat or mouse problem is the only way to fully fix the problem. We have the experience and skills to control the problem and remove the creatures properly and safely.</p>
				<a href="<?php echo URL ?>contact#content" class="btn">LEARN MORE</a>
			</section>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<div class="categories">
			<dl>
				<dt> <img src="public/images/content/cat1.jpg" alt="brown"> </dt>
				<dd>BROWN RATS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/cat2.jpg" alt="black"> </dt>
				<dd>BLACK RATS</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section5">
	<div class="row">
		<div class="s5Left col-6 fl">
			<section>
				<h1>RECENT <br> GALLERY</h1>
				<a href="<?php echo URL ?>contact#content" class="btn">VIEW MORE</a>
			</section>
		</div>
		<div class="s5Right col-6 fl">
			<section>
				<h3>SERVING THE WHOLE BAY AREA </h3>
				<div class="gallery">
					<img src="public/images/content/gallery1.jpg" alt="GALLERY">
					<img src="public/images/content/gallery2.jpg" alt="GALLERY">
					<img src="public/images/content/gallery3.jpg" alt="GALLERY">
					<img src="public/images/content/gallery4.jpg" alt="GALLERY">
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section6">
	<div class="row">
		<div class="s6Left col-6 fl">
			<section>
				<div class="contact">
					<dl>
						<dt> <img src="public/images/content/sprite.png" alt="icons" class="bg-location"> ADDRESS </dt>
						<dd><?php $this->info("address"); ?></dd>
					</dl>
					<dl>
						<dt> <img src="public/images/content/sprite.png" alt="icons" class="bg-phone"> </dt>
						<dd><?php $this->info(["phone","tel"]); ?> - Martin County <br>
								<?php $this->info(["phone2","tel"]); ?> - Palm Beach County <br>
								<?php $this->info(["phone3","tel"]); ?> - Broward County
						</dd>
					</dl>
				</div>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Email Address</span>
						<input type="text" name="email" placeholder="Email Address:">
					</label>
					<label><span class="ctc-hide">Phone Number</span>
						<input type="text" name="phone" placeholder="Phone Number:">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SEND MESSAGE</button>
				</form>
			</section>
		</div>
		<div class="s6Right col-6 fl">
			<img src="public/images/content/img5.png" alt="hunting">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7246422.072317527!2d-88.29649776955627!3d27.52238518332021!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c1766591562abf%3A0xf72e13d35bc74ed0!2sFlorida%2C+USA!5e0!3m2!1sen!2sph!4v1535362237019"  style="border:0; width:100%; height:370px" allowfullscreen></iframe>
